$vcenter = 'vcsa.vsphere.local'
if(Connect-VIServer $vcenter){
    Write-Host 'Configuring ESXi Hosts Lockdown/SSH status'
    
    $HostsToSet = get-view -viewtype hostsystem 
    $SSHService = Get-VMHost | Get-VMHostService | ?{$_.Key -eq 'TSM-SSH'}

    # Enable Lockdown/Disable SSH
    If($HostsToSet | ?{$_.Config.AdminDisabled -like "false"})
    {
        # Secure hosts. Enable Lockdown Mode and disable SSH
        foreach ($ESXhost in $HostsToSet)
            {(get-vmhost $ESXhost | get-view).EnterLockdownMode()} # Enable Lockdown Mode

         # If SSH is running, stop the service
        if($SSHService.Running -eq $true){
            Stop-VMHostService $SSHService -confirm:$false
            }
        # Used for debugging output
        $SSHService = Get-VMHostService $HostsToSet | ?{$_.Key -eq 'TSM-SSH'}
        $HostsToSet | Get-View | Select Name, @{N='LockDownActivated';E={$_.Config.AdminDisabled}}, @{N='SSH Running';E={$SSHService.Running}}, @{N='SSH Policy';E={$SSHService.Policy}} | FT
        Write-Host 'Hosts have been secured'
    }

    # Disable Lockdown Mode and enable SSH
    elseif ($HostsToSet | Get-View | ?{$_.Config.AdminDisabled -like "true"})
    {    
        foreach ($ESXhost in $HostsToSet)
            {(get-vmhost $ESXhost | get-view).ExitLockdownMode()} # To DISABLE Lockdown Mode

        # If SSH is not running, start the service
        if($SSHService.Running -eq $false){
            Start-VMHostService $SSHService -confirm:$false
            }
        # Used for debugging output
        $SSHService = Get-VMHostService $HostsToSet | ?{$_.Key -eq 'TSM-SSH'}
        $HostsToSet | Get-View | Select Name, @{N='LockDownActivated';E={$_.Config.AdminDisabled}}, @{N='SSH Running';E={$SSHService.Running}}, @{N='SSH Policy';E={$SSHService.Policy}} | FT
        Write-Host 'Hosts have been unsecured'
    }
    Write-Host 'Disconnecting from vCenter' $vcenter 
    Disconnect-VIServer $vcenter -confirm:$false
}else {Write-Host 'Unable to connect to vCenter'}
