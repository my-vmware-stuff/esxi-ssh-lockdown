# ESXi SSH Lockdown

Check if SSH is disabled and host lockdown mode is on. If not, disable SSH and enable lockdown mode.